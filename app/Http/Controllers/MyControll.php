<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Input;
use File;
use Hash;
use Auth;
use Carbon\Carbon;
//use Illuminate\Support\Facades\Auth;

class MyControll extends Controller
{
    /*public function  __construct()
    {
        $this->middleware('admin');
    }*/
    public function login()
    {
       return view('login');
    }
   public function home()
   {
       $data  = DB::table('mahasiswa')->count();
       $data2 = DB::table('buku')->count();
       $users = DB::table('mahasiswa')
       ->leftJoin('absen','absen.nrp','=','mahasiswa.nrp')
       ->select('mahasiswa.*','absen.kelas',DB::raw('count(absen.nrp) as status'))
       ->where('absen.pekanstatus','=','1')
       ->groupBy('mahasiswa.nrp','absen.kelas')
       ->get();
       $user = DB::table('mahasiswa')
       ->leftJoin('absen','absen.nrp','=','mahasiswa.nrp')
       ->select('mahasiswa.*','absen.kelas',DB::raw('count(absen.nrp) as status'))
       ->where('absen.pekanstatus','=','0')
       ->having('status','=','2')
       ->orhaving('status','=','3')
       ->groupBy('mahasiswa.nrp','absen.kelas')
       ->get();
       $user1 = DB::table('mahasiswa')
       ->leftJoin('absen','absen.nrp','=','mahasiswa.nrp')
       ->select('mahasiswa.*','absen.kelas',DB::raw('count(absen.nrp) as status'))
       ->where('absen.pekanstatus','=','0')
       ->having('status','>','3')
       ->groupBy('mahasiswa.nrp','absen.kelas')
       ->get();
       $kurang  = DB::table('mahasiswa')->where('pekortu','<','1500000')->count();
       $bidik  = DB::table('mahasiswa')->where('tipe','=','4')->count();
       $pinjam = DB::table('pinjam')->count();
       $kembali = DB::table('kembaliku')->count();             
    // dd($kembali);
       $warning=count($user);
       $drop=count($user1);
    $now = time(); // or your date as well
    $your_date = strtotime("2017-08-21");
    $datediff = $now-$your_date ;
    $datediff= floor($datediff / (60 * 60 * 24*7));
    $jumlah = DB::table('buku')
    ->select('buku.*',DB::raw('sum(buku.jumlah) as jumlah'))
    ->first();            
    $publi1=DB::table('publi')->where('progres','=',0)->count();
    $publi2=DB::table('publi')->where('progres','=',1)->orwhere('progres','=','2')->count();
     $publi3=DB::table('publi')->where('progres','>',2)->count();
// dd($jumlah);
    $curhat=DB::table('curhat')->count();
    // dd($datediff);
    return view('home',compact('data','data2','datediff','warning','drop','kurang','bidik','pinjam','kembali','jumlah','publi1','publi2','publi3','curhat'));
}
public function utama()
{
	return view('utama');
}
public function indexstatus()
{
	$data = DB::table('status')->count();
	$data2 = DB::table('status')->get();
	return view('status.index',compact('data','data2'));
}
public function buatstatus()
{
    return view('status.create');
}
public function masukstatus(Request $request)
{
    $data=DB::table('status')->where('idstat','=',$request->idstatus)->count();
    if($data!=0)
    {
      return Redirect::back()->withErrors(['pesan'=>'Id sudah ada']);
  }
  $data2=DB::table('status')->insert([
    'idstat' => $request->idstatus,
    'namastat'=> $request->namastatus
    ]);
  return Redirect ('/status');
}
public function hapusstatus(Request $request)
{
    $data=DB::table('status')->where('idstat','=',$request->idstatus)->count();
    if($request->akses==1)
    {
       $data=DB::table('status')->where('idstat','=',$request->id)->first();
       return view('status.edit',compact('data'));
   }
   elseif ($request->akses==2)
   {
       $data=DB::table('status')->where('idstat','=',$request->id)->delete();
       return redirect()->route('masuk');
   }
}
public function indexkeluarga()
{
    $data = DB::table('keluarga')->count();
    $data2 = DB::table('keluarga')->get();
    return view ("keluarga.index",compact('data','data2'));
}
public function buatkeluarga()
{
    return view("keluarga.create");
}
public function masukkeluarga(Request $request)
{
    $data=DB::table('keluarga')->where('idke','=',$request->idkeluarga)->count();
    if($data!=0)
    {
        return Redirect::back()->withErrors(['pesan'=>'Id sudah ada']);
    }
    $data2=DB::table('keluarga')->insert([
        'idke' => $request->idkeluarga,
        'namake'=> $request->namakeluarga
        ]);
    return Redirect ('/keluarga');
}
public function hapuskeluarga(Request $request)
{
    if($request->akses==1)
    {
        $data=DB::table('keluarga')->where('idke','=',$request->id)->first();
        return view('keluarga.edit',compact("data"));
    }
    elseif ($request->akses==2)
    {
        $data=DB::table('keluarga')->where('idke','=',$request->id)->delete();
        return redirect()->route('keluarga');
    }
}
public function updatestatus(Request $request)
{
    $data=DB::table('status')->where('idstat','=',$request->id)->update(['idstat'=>$request->id,'namastat'=>$request->namastatus]);
    return redirect()->route('masuk');
}
public function updatekeluarga(Request $request)
{
    $data=DB::table('keluarga')->where('idke','=',$request->id)->update(['idke'=>$request->id,'namake'=>$request->namake]);
    return redirect()->route('keluarga');
}
public function indexmahasiswa()
{
    $data = DB::table('mahasiswa')->count();
    $data2 = DB::table('mahasiswa')
    ->leftJoin('status', 'mahasiswa.tipe', '=', 'status.idstat')
    ->leftJoin('keluarga', 'mahasiswa.keluarga', '=', 'keluarga.idke')
    ->get();         
    $data3= $data2->sortBy('ukt');
    return view ('mahasiswa.index',compact('data','data3'));
}
public function buatmahasiswa()
{
    $data = DB::table('status')->get();
    $data2 = DB::table('keluarga')->get();
    return view ('mahasiswa.create',compact('data','data2'));
}
public function masukmahasiswa(Request $request)
{
    if ($request->hasFile('photo'))
    {
        $extension = Input::file('photo')->getClientOriginalExtension();
        $file="photo_".$request->nrp.'.'.$extension;
        $filefoto=$request->file('photo');
        $filefoto->move('foto/',$file);
        $data = DB::table('mahasiswa')->insert([
            'nrp' => $request->nrp,
            'nama'=> $request->nama,
            'tanggal' => $request->tanggal,
            'lahir' => $request->tempat,
            'domisi' => $request->surabaya,
            'tipe' => $request->beasiswa,
            'keluarga' => $request->keluarga,
            'telp' => $request->telp,
            'ukt' => $request->ukt,
            'kakak' => $request->kakak,
            'adik' => $request->adik,
            'ayah' => $request->namaayah,
            'ibu' => $request->namaibu,
            'alamat' => $request->alamat,
            'pekayah' => $request->pekayah,
            'pekibu' => $request->pekibu,
            'pekortu' => $request->penghasilan,
            'foto' =>$file
            ]);
        return redirect('/mahasiswa');
    }
}
public function hapusmahasiswa(Request $request)
{
    if($request->akses==1)
    {
        $data3 = DB::table('mahasiswa')->where('nrp','=',$request->id)->first();
        $data = DB::table('status')->get();
        $data2 = DB::table('keluarga')->get();
        return view('mahasiswa.edit',compact("data","data2","data3",'history2','history3','history4'));
    }
    elseif ($request->akses==2)
    {
        $data=DB::table('mahasiswa')->where('nrp','=',$request->id)->delete();
        return redirect()->route('mahasiswa');
    }
    else
    {
        $data = DB::table('mahasiswa')->where('nrp','=',$request->id)
        ->leftJoin('status', 'mahasiswa.tipe', '=', 'status.idstat')
        ->leftJoin('keluarga', 'mahasiswa.keluarga', '=', 'keluarga.idke')
        ->first();
        $path=$data->foto;
        $hasil='foto/'.$path;
        return view('mahasiswa.rinci',compact('data','hasil'));
    }
}
public function updatemahasiswa(Request $request)
{
    $data = DB::table('mahasiswa')->where('nrp','=',$request->id)->first();
    if($request->hasFile('photo'))
    {
        File::Delete('foto/'.$data->foto);
        $extension = Input::file('photo')->getClientOriginalExtension();
        $file="photo_".$request->id.'.'.$extension;
        $filefoto=$request->file('photo');
        $filefoto->move('foto/',$file);
        // dd($request->id);
        $data=DB::table('mahasiswa')->where('nrp','=',$request->id)->update([
            'nama'=>$request->nama,
            'tanggal' => $request->tanggal,
            'lahir' => $request->tempat,
            'domisi' => $request->surabaya,
            'tipe' => $request->beasiswa,
            'keluarga' => $request->keluarga,
            'telp' => $request->telp,
            'ukt' => $request->ukt,
            'kakak' => $request->kakak,
            'adik' => $request->adik,
            'ayah' => $request->namaayah,
            'ibu' => $request->namaibu,
            'alamat' => $request->alamat,
            'pekayah' => $request->pekayah,
            'pekibu' => $request->pekibu,
            'pekortu' => $request->penghasilan,
            'foto' =>$file
            ]);
        return redirect()->route('mahasiswa');
    }
}
public function masuk(Request $request)
{
    if (Auth::attempt(['email' => $request->nrp, 'password' => $request->password])) 
    {
        DB::table('login')->insert([
            'nrp' => $request->nrp,
            'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
            ]);
        return redirect()->route('dashboard');
        //return Auth::user()->admin;
        //return "cek";
    }
    else
    {
        //return "cek";
        return redirect()->route('login');
    }

}
public function akun()
{
    return view('create');
}
public function isi(Request $request)
{
    $time =Carbon::now()->format('Y-m-d H:i:s');
    $password = Hash::make($request->pass);
    $data = DB::table('users')->insert([
        'email' => $request->nrp,
        'name'=> $request->nama,
        'password' =>$password,
        'admin' => $request->status,
        'remember_token' => $request->_token,
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);
    return redirect()->route('login');
}
public function keluar()
{
    Auth::logout();
    return redirect()->route('login');
}
public function lihat(Request $request)
{
    $data=DB::table('mahasiswa')->where('nrp','=',$request->nrp)->first();
    dd($request->nrp);
    return view('mahasiswa.rinci',compact('data'));
}
public function input()
{
    $data = DB::table('status')->get();
    $data2 = DB::table('keluarga')->get();
    return view('input',compact('data','data2'));
}
public function man ()
{
    $user_info = DB::table('mahasiswa')
    ->select('keluarga', DB::raw('count(*) as total'))
    ->groupBy('keluarga')
    ->get();
}
public function masukinput(Request $request)
{
    if ($request->hasFile('foto'))
    {
        $extension = Input::file('foto')->getClientOriginalExtension();
        $file="photo_".$request->nrp.'.'.$extension;
        $filefoto=$request->file('foto');
        $filefoto->move('foto/',$file);
        $data = DB::table('mahasiswa')->insert([
            'nrp' => $request->nrp,
            'nama'=> $request->nama,
            'tanggal' => $request->tanggal,
            'lahir' => $request->tempat,
            'domisi' => $request->surabaya,
            'tipe' => $request->beasiswa,
            'keluarga' => $request->keluarga,
            'telp' => $request->telp,
            'ukt' => $request->ukt,
            'kakak' => $request->kakak,
            'adik' => $request->adik,
            'ayah' => $request->namaayah,
            'ibu' => $request->namaibu,
            'alamat' => $request->alamat,
            'pekayah' => $request->pekayah,
            'pekibu' => $request->pekibu,
            'pekortu' => $request->penghasilan,
            'foto' =>$file
            ]);
        return redirect('/login');
    }
    else
    {
        echo "string";
    }
}
public function indexbuku()
{
    $data=DB::table('buku')->count();
    $data2=DB::table('buku')->get();
    // $users = DB::table('buku')
    //              ->leftJoin('pinjam','buku.idbuku','=','pinjam.idbuku')
    //              ->select('buku.*',DB::raw('count(pinjam.kembali) as dipinjam'))
    //              ->groupBy('buku.idbuku')
    //              ->get();
    $users = DB::table('buku')
    ->leftJoin('pinjam','buku.idbuku','=','pinjam.idbuku')
    ->leftJoin('kembaliku','buku.idbuku','=','kembaliku.buku')
    ->select('buku.*',DB::raw('count(pinjam.kembali) as dipinjam'),DB::raw('count(kembaliku.buku) as kembali'))
    ->groupBy('buku.idbuku')
    ->get();
    return view('buku.index',compact('data','data2','users'));
}
public function buatbuku()
{
    return view ('buku.create');
}
public function masukbuku(Request $request)
{
    if ($request->hasFile('foto'))
    {
        $extension = Input::file('foto')->getClientOriginalExtension();
        $file="Buku_".$request->idbuku.'.'.$extension;
        $filefoto=$request->file('foto');
        $filefoto->move('foto/buku',$file);
        DB::table('buku')->insert([
            'idbuku' => $request->idbuku,
            'namabuku'=> $request->nama,
            'jumlah'=> $request->jumlah,
            'Edisi' => $request->edi,
            'foto' =>$file
            ]);
        return redirect()->route('buku');
    }
    else
    {
        echo "string";
    }
}
public function hapusbuku(Request $request)
{
    if($request->akses==1)
    {
        $data=DB::table('buku')->where('idbuku','=',$request->id)->first();
        return view('buku.edit',compact("data"));
    }
    elseif ($request->akses==2)
    {
        $data=DB::table('buku')->where('idbuku','=',$request->id)->delete();
        return redirect()->route('buku');
    }
}
public function updatebuku(Request $request)
{
    if ($request->hasFile('foto'))
    {
        $extension = Input::file('foto')->getClientOriginalExtension();
        $file="Buku_".$request->id.'.'.$extension;
        $filefoto=$request->file('foto');
        $filefoto->move('foto/buku',$file);
        $data=DB::table('buku')->where('idbuku','=',$request->id)->update([
            'idbuku' => $request->id,
            'namabuku'=> $request->buku,
            'jumlah'=> $request->jumlah,
            'foto' =>$file
            ]);
        return redirect()->route('buku');
    }
    else
    {
        echo "string";
    }
}
public function indexpinjam()
{
    $data = DB::table('pinjam')->count();
    $data2 = DB::table('pinjam')
    ->leftJoin('buku', 'buku.idbuku', '=', 'pinjam.idbuku')
    ->get();
    return view ("pinjam.index",compact('data','data2'));
}
public function buatpinjam()
{
    $nama= DB::table('mahasiswa')->get();
    $buku = DB::table('buku')->get();
    return view('pinjam.create',compact('buku','nama'));
}
public function masukpinjam(Request $request)
{
    DB::table('pinjam')->insert([
        'nrp' => $request->nrp,
        'idbuku'=> $request->buku,
        'Idnya' => $request->id,
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);
    return redirect()->route('pinjam');
}
public function hapuspinjam(Request $request)
{

    if($request->akses==1)
    {
        $data=DB::table('pinjam')->where('id','=',$request->id)->first();
        $buku = DB::table('buku')->get();
        // dd($data);
        return view('pinjam.edit',compact('buku','data'));
    }
    elseif ($request->akses==2)
    {
        $data=DB::table('pinjam')->where('id','=',$request->id)->delete();

        return redirect()->route('pinjam');
    }
    elseif ($request->akses==3)
    {
       $data=DB::table('pinjam')->where('id','=',$request->id)->update(['kembali'=>'1']);
       DB::table('kembaliku')->insert([
        'pinjam' => $request->id,
        'buku'=> $request->idbuku,
        ]);
       return redirect()->route('pinjam');
   }
   else
   {
    $data=DB::table('pinjam')->where('id','=',$request->id)->update(['kembali'=>'0']);
    $data1=DB::table('kembaliku')->where('pinjam','=',$request->id)->delete();
    return redirect()->route('pinjam');
}
}
public function updatepinjam(Request $request)
{
    $data=DB::table('pinjam')->where('id','=',$request->id)->update(['nrp'=>$request->nrp,'idbuku'=>$request->buku]);
     // dd($request->id);
    return redirect()->route('pinjam');
}
public function buatkelas()
{
    return view('kelas.create');
}
public function masukkelas(Request $request)
{
    $data2=DB::table('kelas')->insert([
        'kelas' => $request->namakelas,
        'ruangan' => $request->namaruangan,
        'semester' => $request->semester,
        'jam' =>$request->jam
        ]);
    return redirect()->route('kelas');
}
public function indexkelas()
{
    $data = DB::table('kelas')->count();
    $data2 = DB::table('kelas')->get();
    return view ('kelas.index',compact('data','data2'));
}
public function hapuskelas(Request $request)
{

    if($request->akses==1)
    {
        $data = DB::table('kelas')->where('idkelas','=',$request->id)->first();
        return view('kelas.edit',compact('data'));
    }
    elseif ($request->akses==2)
    {
        $data=DB::table('kelas')->where('idkelas','=',$request->id)->delete();
        return redirect()->route('kelas');
    }
}
public function updatekelas(Request $request)
{
     // $data=DB::table('kelas')->where('idkelas','=',$request->id)->get();
     // dd($request->id);
    $data=DB::table('kelas')->where('idkelas','=',$request->id)->update([
        'kelas' => $request->namakelas,
        'ruangan' => $request->namaruangan,
        'semester' => $request->semester,
        'jam' =>$request->jam
        ]);

    return redirect()->route('kelas');
}
public function buatmember()
{
    $data=DB::table('kelas')->get();
    return view ('member.create',compact('data'));
}
public function masukmember(Request $req)
{
    $data2=DB::table('member')->insert([
        'kelas' => $req->namakelas,
        'nrp'=> $req->nrp
        ]);
    return redirect()->route('member');
}
public function indexmember()
{
    $a=Input::get('kelas');
    $list=DB::table('kelas')->get();
    if($a==null||$a=="")
    {
        $data2=DB::table('member')->leftJoin('kelas','member.kelas','kelas.idkelas')->leftJoin('mahasiswa','mahasiswa.nrp','member.nrp')->get()->sortBy('kelas');
        $data=DB::table('member')->count();
    // dd($data2);
        return view ('member.index',compact('data','data2','list'));
    }
    else
    {
        $data2=DB::table('member')->leftJoin('kelas','member.kelas','kelas.idkelas')->leftJoin('mahasiswa','mahasiswa.nrp','member.nrp')->where('member.kelas','=',$a)->get()->sortBy('kelas');
        $data=DB::table('member')->count();
        return view ('absen.index',compact('data','data2','list'));
    }
}
public function hapusmember(Request $request)
{

    if($request->akses==1)
    {
        $data2 = DB::table('member')->where('id','=',$request->id)->first();
        $data = DB::table('kelas')->get();
        return view('member.edit',compact('data','data2'));
    }
    elseif ($request->akses==2)
    {
        $data=DB::table('member')->where('id','=',$request->id)->delete();
        return redirect()->route('member');
    }
}
public function updatemember(Request $req)
{
     // $data=DB::table('kelas')->where('idkelas','=',$request->id)->get();
     // dd($request->id);
    // dd($req->id);
    $data=DB::table('member')->where('id','=',$req->id)->update([
        'kelas' => $req->namakelas,
        'nrp'=> $req->nrp
        ]);

    return redirect()->route('member');
}
public function indexabsen()
{
    $a=Input::get('kelas');
    $list=DB::table('kelas')->get();
    if($a==null||$a=="")
    {
        $data2=DB::table('member')->leftJoin('kelas','member.kelas','kelas.idkelas')->leftJoin('mahasiswa','mahasiswa.nrp','member.nrp')->get()->sortBy('kelas');
        $data=DB::table('member')->count();
        // dd($data2);
        return view ('absen.index',compact('data','data2','list'));  
    }
    else
    {
        // echo $a;
        $data2=DB::table('member')->leftJoin('kelas','member.kelas','kelas.idkelas')->leftJoin('mahasiswa','mahasiswa.nrp','member.nrp')->where('member.kelas','=',$a)->get()->sortBy('kelas');
        $data=DB::table('member')->count();
        return view ('absen.index',compact('data','data2','list'));  
       // return $a;
    }
}
public function hapus_absen(Request $request)
{

    if($request->akses==1)
    {
        $data2=DB::table('absen')->where('nrp','=',$request->id)->where('kelas','=',$request->idkelas)->count();
        $data=DB::table('absen')->where('nrp','=',$request->id)->where('kelas','=',$request->idkelas)->get();
        // dd($data);
        $data3=DB::table('member')->where('member.nrp','=',$request->id)->where('member.kelas','=',$request->idkelas)->leftJoin('mahasiswa','mahasiswa.nrp','member.nrp')->leftJoin('kelas','member.kelas','kelas.idkelas')->first();
        $data4=DB::table('absen')->where('nrp','=',$request->id)->where('kelas','=',$request->idkelas)->get();
        // dd($data3);
        return view('absen.rinci',compact('data','data2','data3','data4','dataku'));
    }
}
public function rinciabsen(Request $request)
{
    return redirect()->route('member');
}
public function pekan(Request $request)
{
    if($request->akses==1)
    {
        $data=DB::table('absen')->where('id','=',$request->id)->update(['pekanstatus'=>'1']);
        return redirect()->route('absen');
    }
    elseif ($request->akses==2)
    {
       $data=DB::table('absen')->where('id','=',$request->id)->update(['pekanstatus'=>'0']);
       return redirect()->route('absen');
   }
   elseif ($request->akses==3)
   {
       $data=DB::table('absen')->where('id','=',$request->id)->delete();
       return redirect()->route('absen');
   }
}
public function tambah(Request $req)
{
    // dd($req->idkelas);
    if($req->akses==1)
    {
        $data2=DB::table('absen')->insert([
            'nrp' => $req->id,
            'kelas'=> $req->idkelas,
            'pekan'=> $req->pekan,
            'pekanstatus'=>'1'
            ]);
    }
    else
    {
        $data2=DB::table('absen')->insert([
            'nrp' => $req->id,
            'kelas'=> $req->idkelas,
            'pekan'=> $req->pekan,
            'pekanstatus' =>'0'
            ]);

    }
    return redirect()->route('absen');
}
public function warning()
{
    $user = DB::table('mahasiswa')
       ->leftJoin('absen','absen.nrp','=','mahasiswa.nrp')
       ->leftJoin('kelas','kelas.idkelas','absen.kelas')
       ->select('mahasiswa.*','absen.kelas','kelas.*',DB::raw('count(absen.nrp) as status'))
       ->where('absen.pekanstatus','=','0')
       ->having('status','=','2')
       ->orhaving('status','=','3')
       ->groupBy('mahasiswa.nrp','absen.kelas')
       ->get();
    $data=count($user);
    // dd($user);
    return view('warning',compact('data','user'));
}

public function drop()
{
    $user = DB::table('mahasiswa')
       ->leftJoin('absen','absen.nrp','=','mahasiswa.nrp')
       ->leftJoin('kelas','kelas.idkelas','absen.kelas')
       ->select('mahasiswa.*','absen.kelas','kelas.*',DB::raw('count(absen.nrp) as status'))
       ->where('absen.pekanstatus','=','0')
       ->having('status','>','3')
       ->groupBy('mahasiswa.nrp','absen.kelas')
       ->get();
    $data=count($user);
    // dd($user);
    return view('drop',compact('data','user'));
}

public function kurang()
{
    $kurang  = DB::table('mahasiswa')->where('pekortu','<','1500000')->count();
    $kurang1 = DB::table('mahasiswa')->where('pekortu','<','1500000')
    ->leftJoin('status','status.idstat','mahasiswa.tipe')
    ->leftJoin('keluarga','keluarga.idke','mahasiswa.keluarga')
    ->get();
    return view('kurang',compact('kurang','kurang1'));
}

public function bidik()
{
     $kurang  = DB::table('mahasiswa')->where('tipe','=','4')->count();
      $kurang1 = DB::table('mahasiswa')->where('tipe','=','4')
    ->leftJoin('status','status.idstat','mahasiswa.tipe')
    ->leftJoin('keluarga','keluarga.idke','mahasiswa.keluarga')
    ->get();
    return view ('kurang',compact('kurang1','kurang'));
}
public function sudah()
{
    $data = DB::table('pinjam')->where('kembali','=','1')->count();
    $data2 = DB::table('pinjam')
    ->leftJoin('mahasiswa', 'mahasiswa.nrp', '=', 'pinjam.nrp')
    ->leftJoin('buku', 'buku.idbuku', '=', 'pinjam.idbuku')
    ->where('kembali','=','1')
    ->get();
    return view ("pinjam.index",compact('data','data2'));    
}
public function belum()
{
    $data = DB::table('pinjam')->where('kembali','=','0')->count();
    $data2 = DB::table('pinjam')
    ->leftJoin('mahasiswa', 'mahasiswa.nrp', '=', 'pinjam.nrp')
    ->leftJoin('buku', 'buku.idbuku', '=', 'pinjam.idbuku')
    ->where('kembali','=','0')
    ->get();
    return view ("pinjam.index",compact('data','data2'));    
}
public function publi()
{
    $data = DB::table('publi')->count();
    $data2 = DB::table('publi')->get();
    return view ('beasiswa.index',compact('data','data2'));
}
public function hapuspubli(Request $request)
{
     if($request->akses==0)
    {
        $data=DB::table('publi')->where('id','=',$request->id)->update(['progres'=>'0']);
        return redirect()->route('publi');
    }
    elseif ($request->akses==1)
    {
       $data=DB::table('publi')->where('id','=',$request->id)->update(['progres'=>'1']);
       return redirect()->route('publi');
    }
    elseif ($request->akses==2)
    {
       $data=DB::table('publi')->where('id','=',$request->id)->update(['progres'=>'2']);
       return redirect()->route('publi');
    }
     elseif ($request->akses==3)
    {
       $data=DB::table('publi')->where('id','=',$request->id)->update(['progres'=>'3']);
       return redirect()->route('publi');
    }
    elseif ($request->akses==4)
    {
       $data=DB::table('publi')->where('id','=',$request->id)->delete();
       return redirect()->route('publi');
    }
    else
    {
        $data=DB::table('publi')->where('id','=',$request->id)->first();
        return view('beasiswa.edit',compact('data'));
    }
}
public function buatpubli()
{
    return view('beasiswa.create');
}
public function masukpubli(Request $request)
{
    $data2=DB::table('publi')->insert([
    'namabeasiswa' => $request->nama,
    'tanggal'=> $request->tanggal,
    'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
    ]);
    return redirect()->route('publi');
}
public function updatepubli(Request $request)
{
    $data=DB::table('publi')
    ->where('id','=',$request->id)
    ->update(['id'=>$request->id,
        'namabeasiswa' => $request->nama,
    'tanggal'=> $request->tanggal,
    'keterangan'=>$request->keterangan,
    'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')
    ]);
    return redirect()->route('publi');
}
public function belumpubli()
{
     $data = DB::table('publi')->where('progres','=',0)->count();
    $data2 = DB::table('publi')->where('progres','=',0)->get();
    return view ('beasiswa.index',compact('data','data2'));
}
public function medfo()
{
     $data = DB::table('publi')->where('progres','=',1)->orwhere('progres','=','2')->count();
    $data2 = DB::table('publi')->where('progres','=',1)->orwhere('progres','=','2')->get();
    return view ('beasiswa.index',compact('data','data2'));
}
public function bebas()
{
     $data = DB::table('publi')->where('progres','=',3)->count();
    $data2 = DB::table('publi')->where('progres','=',3)->get();
    return view ('beasiswa.index',compact('data','data2'));
}
public function calendar()
{
    return view('calendar');
}
public function halo()
{
    $data2 = DB::table('mahasiswa')
    ->leftJoin('status', 'mahasiswa.tipe', '=', 'status.idstat')
    ->leftJoin('keluarga', 'mahasiswa.keluarga', '=', 'keluarga.idke')
    ->get();         
    $data3= $data2->sortBy('ukt');
    return view ('live',compact('data3'));
}
public function search(Request $request)
{
    if($request->ajax())
    {
        $output="";
        $sec="";
        $mahasiswa=DB::table('mahasiswa')->leftJoin('status', 'mahasiswa.tipe', '=', 'status.idstat')
    ->leftJoin('keluarga', 'mahasiswa.keluarga', '=', 'keluarga.idke')->where('nama','LIKE','%'.$request->search.'%')->get();

        if($mahasiswa)
        {
            foreach ($mahasiswa as $key => $mahasiswa) 
            {
                $output.='<tr>'.
                        '<td>'.$mahasiswa->nrp.'</td>'.
                        '<td>'.$mahasiswa->nama.'</td>'.
                        '<td>'.$mahasiswa->ukt.'</td>'.
                        '<td>'.$mahasiswa->namastat.'</td>'.
                        '<td>'.$mahasiswa->namake.'</td>'.
                        '<td>'.$mahasiswa->pekortu.'</td>'.
                        '</tr>';
            }
            return Response($output);
        }
    }
}
public function caribuku()
{
    return view('caribuku');
}
public function masukcari(Request $request)
{
    $users = DB::table('buku')
    ->leftJoin('pinjam','buku.idbuku','=','pinjam.idbuku')
    ->leftJoin('kembaliku','buku.idbuku','=','kembaliku.buku')
    ->where('namabuku','LIKE','%'.$request->nama.'%')
    ->get();
    $users = DB::table('buku')
    ->leftJoin('pinjam','buku.idbuku','=','pinjam.idbuku')
    ->leftJoin('kembaliku','buku.idbuku','=','kembaliku.buku')
    ->select('buku.*',DB::raw('count(pinjam.kembali) as dipinjam'),DB::raw('count(kembaliku.buku) as kembali'))
    ->groupBy('buku.idbuku')
    ->where('namabuku','LIKE','%'.$request->nama.'%')
    ->get();
    // dd($users);
    return view('temubuku',compact('users'));
}
public function curhat()
{
    return view ('curhat');
}
public function masukcurhat(Request $request)
{
     $data2=DB::table('curhat')->insert([
    'nrp' => $request->nrp,
    'nama'=> $request->nama,
    'topik' =>$request->topik,
    'curhat' => $request->curhat
    ]);
     return redirect()->route('depan');
}
public function lihatcur()
{
    $data=DB::table('curhat')->count();
    $data2=DB::table('curhat')->get();
    return view('dipalcur',compact('data2','data'));
}
}
