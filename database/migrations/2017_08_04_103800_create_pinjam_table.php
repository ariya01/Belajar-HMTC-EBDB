<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinjamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjam', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nrp');
            $table->string('idbuku');
            $table->boolean('kembali')->default('0');
            $table->timestamps();


            $table->foreign('nrp')
            ->references('nrp')->on('mahasiswa')
            ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('idbuku')
            ->references('idbuku')->on('buku')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjam');
    }
}
