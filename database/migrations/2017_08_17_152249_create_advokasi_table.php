<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvokasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advokasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nrp');
            $table->string('alasan')->nullable();
            $table->timestamps();

             $table->foreign('nrp')
            ->references('nrp')->on('mahasiswa')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advokasi');
    }
}
