<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('kelas');
            $table->string('nrp');
            $table->timestamps();

            $table->foreign('kelas')
            ->references('idkelas')->on('kelas')
            ->onDelete('cascade')->onUpdate('cascade');

             $table->foreign('nrp')
            ->references('nrp')->on('mahasiswa')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
