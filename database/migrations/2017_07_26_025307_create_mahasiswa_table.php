<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
           $table->string('nrp');
           $table->primary('nrp');
           $table->string('nama');
           $table->date('tanggal');
           $table->string('lahir');
           $table->string('domisi');
           $table->string('tipe');
           $table->string('keluarga');
           $table->string('telp');
           $table->integer('ukt');
           $table->integer('kakak');
           $table->integer('adik');
           $table->string('Ayah');
           $table->string('Ibu');
           $table->string('alamat');
           $table->string('pekayah');
           $table->string('pekibu');
           $table->integer('pekortu');
           $table->string('foto');

           $table->foreign('tipe')
            ->references('idstat')->on('status')
            ->onDelete('cascade')->onUpdate('cascade');

             $table->foreign('keluarga')
            ->references('idke')->on('keluarga')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa');
    }
}
