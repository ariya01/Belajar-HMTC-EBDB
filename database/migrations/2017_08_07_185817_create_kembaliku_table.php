<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKembalikuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kembaliku', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pinjam');
            $table->string('buku');
            $table->timestamps();

             $table->foreign('pinjam')
            ->references('id')->on('pinjam')
            ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('buku')
            ->references('idbuku')->on('buku')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kembaliku');
    }
}
