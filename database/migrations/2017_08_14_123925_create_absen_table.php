<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nrp');
            $table->unsignedInteger('kelas');
            $table->string('pekan');
            $table->integer('pekanstatus');

            $table->foreign('kelas')
            ->references('idkelas')->on('kelas')
            ->onDelete('cascade')->onUpdate('cascade');

             $table->foreign('nrp')
            ->references('nrp')->on('mahasiswa')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absen');
    }
}
