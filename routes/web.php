<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MyControll@utama')->name('depan');
Route::get('/login','MyControll@login');
Route::get('/input','MyControll@input');
Route::post('/masuk_input','MyControll@masukinput');
Route::post('/login','MyControll@masuk')->name('login');
Route::get('cari_buku','MyControll@caribuku');
Route::post('masuk_cari','MyControll@masukcari');
Route::get('curhat','MyControll@curhat');
Route::post('masuk_curhat','MyControll@masukcurhat');

Route::group(['middleware' => 'admin'], function () {
	Route::get('/home','MyControll@home')->name('dashboard');
	Route::get('/status','MyControll@indexstatus')->name('masuk');
	Route::get('/buat_status','MyControll@buatstatus');
	Route::post('/masuk_status','MyControll@masukstatus');
	Route::post('/hapus_status','MyControll@hapusstatus');
	Route::get('/keluarga','MyControll@indexkeluarga')->name('keluarga');
	Route::get('/buat_keluarga','MyControll@buatkeluarga');
	Route::post('/masuk_keluarga','MyControll@masukkeluarga');
	Route::post('/hapus_keluarga','MyControll@hapuskeluarga');
	Route::post('/update_status','MyControll@updatestatus');
	Route::post('/update_keluarga','MyControll@updatekeluarga');
	Route::get('/mahasiswa','MyControll@indexmahasiswa')->name('mahasiswa');
	Route::get('/buat_mahasiswa','MyControll@buatmahasiswa');
	Route::post('/masuk_mahasiswa','MyControll@masukmahasiswa');
	Route::post('/hapus_mahasiswa','MyControll@hapusmahasiswa');
	Route::post('/update_mahasiswa','MyControll@updatemahasiswa');
	Route::get('/keluar','MyControll@keluar')->name('keluar');	
	Route::post('isi','MyControll@isi');
	Route::post('/lihat','MyControll@lihat');
	Route::get('/buat_buku','MyControll@buatbuku');
	Route::post('/masuk_buku','MyControll@masukbuku');
	Route::get('buku','MyControll@indexbuku')->name('buku');
 	Route::post('hapus_buku','MyControll@hapusbuku');
	Route::post('update_buku','MyControll@updatebuku');
	Route::get('/pinjam','MyControll@indexpinjam')->name('pinjam');
	Route::get('/buat_pinjam','MyControll@buatpinjam');
	Route::post('/masuk_pinjam','MyControll@masukpinjam');
	Route::post('hapus_pinjam','MyControll@hapuspinjam');
	Route::post('update_pinjam','MyControll@updatepinjam');
	Route::get('buat_kelas','MyControll@buatkelas');
	Route::post('masuk_kelas','MyControll@masukkelas');
	Route::get('/kelas','MyControll@indexkelas')->name('kelas');
	Route::post('/hapus_kelas','MyControll@hapuskelas');
	Route::get('/buat_member','MyControll@buatmember');
	Route::post('/masuk_member','MyControll@masukmember');
	Route::get('/member','MyControll@indexmember')->name('member');
	Route::post('hapus_member','MyControll@hapusmember');
	Route::post('update_kelas','MyControll@updatekelas');
	Route::post('update_member','MyControll@updatemember');
	Route::get('/absen','MyControll@indexabsen')->name('absen');
	Route::post('/hapus_absen','MyControll@hapus_absen');
	Route::get('rinci_absen','MyControll@rinciabsen')->name('rinci');
	Route::post('pekan','MyControll@pekan');
	Route::post('/tambah','MyControll@tambah')->name('tambah');
	Route::get('warning','MyControll@warning');
	Route::get('drop','MyControll@drop');
	Route::get('kurang','MyControll@kurang');
	Route::get('bidik','MyControll@bidik');
	Route::get('sudah','MyControll@sudah');
	Route::get('belum','MyControll@belum');
	Route::get('publi','MyControll@publi')->name('publi');
	Route::post('hapus_publi','MyControll@hapuspubli');
	Route::get('buat_publi','MyControll@buatpubli');
	Route::post('masuk_publi','MyControll@masukpubli');
	Route::post('update_publi','MyControll@updatepubli');
	Route::get('belum_publi','MyControll@belumpubli');
	Route::get('medfo','MyControll@medfo');
	Route::get('bebas','MyControll@bebas');
	Route::get('calendar','MyControll@calendar');
	Route::get('search','MyControll@search');
	Route::get('/live','MyControll@halo');	
	Route::get('lihatcur','MyControll@lihatcur');
});