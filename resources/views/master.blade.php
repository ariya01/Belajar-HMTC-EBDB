<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
  <link rel="manifest" href="/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <title>KESMA HMTC</title>
  <style type="text/css"> @yield('css')</style>
  <!-- Bootstrap core CSS -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <!--external css-->
  <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css"> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   

  <!-- Custom styles for this template -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/style-responsive.css" rel="stylesheet">
  <script src="assets/js/chart-master/Chart.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>

      <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
        <div class="sidebar-toggle-box">
          <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="index.html" class="logo"><b>KESMA HMTC</b></a>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
        <img class="img-center img-responsive" src="images/head.png" style="margin-top: -12px;">
      </div>
      <div class="top-menu">
       <ul class="nav pull-right top-menu">
        <li><a class="logout" href="{{action('MyControll@keluar')}}">Logout</a></li>
      </ul>
    </div>
  </header>
  <!--header end-->

      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
        <div id="sidebar"  class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
           <h5 class="centered">{{Auth::user()->name}}</h5>

           <li class="mt">
            <a @yield('home') href="{{url('home')}}">
              <i class="fa fa-dashboard"></i>
              <span>Home</span>
            </a>
          </li>
          <hr>
           <h5 class="centered">Mahasiswa</h5>
          <li class="sub-menu">
            <a @yield('mahasiswa') href="javascript:;" >
              <i class="fa fa-book"></i>
              <span>Mahasiswa</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('mahasiswa')}}">Daftar Mahasiswa</a></li>
              <li><a  href="{{url('buat_mahasiswa')}}">Tambah Mahasiswa</a></li>
              <li><a  href="{{url('live')}}">Cari Mahasiswa</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a @yield('status') href="javascript:;" >
              <i class="fa fa-desktop"></i>
              <span>Status</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('status')}}">Daftar Status</a></li>
              <li><a  href="{{url('buat_status')}}">Tambah Status</a></li>
            </ul>
          </li>

          <li class="sub-menu">
            <a @yield('keluarga') href="javascript:;" >
              <i class="fa fa-cogs"></i>
              <span>Keluarga</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('keluarga')}}">Daftar Keluarga</a></li>
              <li><a  href="{{url('buat_keluarga')}}">Tambah Keluarga</a></li>
            </ul>
          </li>
          <hr>
          <h5 class="centered">Buku</h5>
          <li class="sub-menu">
            <a @yield('buku') href="javascript:;" >
              <i class="fa fa-book"></i>
              <span>Buku</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('buku')}}">Daftar Buku</a></li>
            </ul>
            <ul class="sub">
              <li><a  href="{{url('buat_buku')}}">Tamabah Buku</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a @yield('pinjam') href="javascript:;" >
              <i class="fa fa-th"></i>
              <span>Peminjam</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('pinjam')}}">Daftar Peminjam</a></li>
              <li><a  href="{{url('buat_pinjam')}}">Tambah Peminjam</a></li>
            </ul>
          </li>
          <hr>
          <h5 class="centered">Daftar Absen</h5>
          <li class="sub-menu">
            <a @yield('kelas') href="javascript:;" >
              <i class=" fa fa-bar-chart-o"></i>
              <span>Kelas</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('kelas')}}">Daftar Kelas</a></li>
              <li><a  href="{{url('buat_kelas')}}">Tambah Kelas</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a @yiled('member') href="javascript:;" >
              <i class=" fa fa-bar-chart-o"></i>
              <span>Member</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('member')}}">Member</a></li>
              <li><a  href="{{url('buat_member')}}">Tambah Member</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a @yield('absen') href="javascript:;" >
              <i class=" fa fa-bar-chart-o"></i>
              <span>Absensi</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('absen')}}">Absensi</a></li>
            </ul>
          </li>
          <hr>
          <h5 class="centered">Publikasi Beasiswa</h5>
          <li class="sub-menu">
            <a @yield('advo') href="javascript:;" >
              <i class="fa fa-book"></i>
              <span>Beasiswa</span>
            </a>
            <ul class="sub">
              <li><a  href="{{url('publi')}}">Daftar Beasiswa</a></li>
              <li><a  href="{{url('buat_publi')}}">Tambah Beasiswa</a></li>
            </ul>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">

          <div class="row">
            <div class="col-lg-12 main-chart">
            @yield('content')
            </div><!-- /col-lg-9 END SECTION MIDDLE -->


      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                   
      @yield('login')
      

<!-- USERS ONLINE SECTION -->

</section>
</section>

<!--main content end-->
<!--footer start-->
<!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-1.8.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="assets/js/jquery.sparkline.js"></script>


<!--common script for all pages-->
<script src="assets/js/common-scripts.js"></script>

<script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="assets/js/gritter-conf.js"></script>

<!--script for this page-->
<script src="assets/js/sparkline-chart.js"></script>    
<script src="assets/js/zabuto_calendar.js"></script>	


<script type="application/javascript">
  $(document).ready(function () {
    $("#date-popover").popover({html: true, trigger: "manual"});
    $("#date-popover").hide();
    $("#date-popover").click(function (e) {
      $(this).hide();
    });

    $("#my-calendar").zabuto_calendar({
      action: function () {
        return myDateFunction(this.id, false);
      },
      action_nav: function () {
        return myNavFunction(this.id);
      },
      
      ajax: {
        url: "show_data.php?action=1",
        modal: true
      },
      legend: [
      {type: "text", label: "Special event", badge: "00"},
      {type: "block", label: "Regular event", }
      ]
    });
  });


  function myNavFunction(id) {
    $("#date-popover").hide();
    var nav = $("#" + id).data("navigation");
    var to = $("#" + id).data("to");
    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
  }
</script>


</body>
</html>
