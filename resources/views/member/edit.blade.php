@extends('master')
@section('content')
<h3 class=" text-center">Form Anggota</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('update_member')}}">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Kelas</label>
					<div class="col-sm-10">
						<select class="form-control round-form" name="namakelas" id="beasiswa">
			              <option value="0" disabled="true" selected="true">Pilih Kelas</option>
			              @foreach($data as $a)
			              <option value="{{$a->idkelas}}">{{$a->kelas}}</option>
			              @endforeach
			            </select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">NRP</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="nrp" placeholder="{{$data2->nrp}}">
					</div>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{$data2->id}}"></input>
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
