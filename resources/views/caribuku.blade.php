<!DOCTYPE html>
<html lang="en">
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
  <link rel="manifest" href="/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <title>KESMA HMTC</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<!--external css-->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

	<!-- Custom styles for this template -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/style-responsive.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body>
  	<div id="login-page">
  		<div class="container">
  			<form class="form-login1" method="post" action="{{url('masuk_cari')}}" enctype="multipart/form-data">
  				<h2 class="form-login-heading">Cari Buku</h2>
  				<div class="login-wrap">
  					<label class="control-label">Nama Buku</label>
  					<input type="text" class="form-control round-form" name="nama">
  					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <br>
  					<button class="btn btn-theme btn-block" type="submit">Cari</button>
  				</div>
  			</form>       

  		</div>
  	</div>

  	<!-- js placed at the end of the document so the pages load faster -->
  	<script src="assets/js/jquery.js"></script>
  	<script src="assets/js/bootstrap.min.js"></script>

  	<!--BACKSTRETCH-->
  	<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  	<script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
  	<script>
  		$.backstretch("images/banner.jpg", {speed: 500});
  	</script>
  </body>
  </html>
