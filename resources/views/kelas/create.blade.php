@extends('master')
@section('content')
<h3 class=" text-center">Form Kelas</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('masuk_kelas')}}">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Kelas</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="namakelas">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Ruangan Kelas</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="namaruangan">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Semester</label>
					<div class="col-sm-10">
						<select class="form-control round-form" name="semester">
			              <option value="0" disabled="true" selected="true">Pilih Semester</option>
			              <option value="1">Ganjil</option>
			              <option value="2">Genap</option>
			            </select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Jam Kelas</label>
					<div class="col-sm-10">
						<input type="time" class="form-control round-form" name="jam">
					</div>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
