@extends('master')
@section('content')
<h3 class=" text-center">Edit Status</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('update_buku')}}" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Id Buku</label>
					<div class="col-sm-10">
						<input class="form-control round-form" id="disabledInput" type="text" placeholder="{{$data->idbuku}}" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Buku</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="buku" placeholder="{{$data->namabuku}}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Jumlah Buku</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="jumlah" placeholder="{{$data->jumlah}}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Foto</label>
					<div class="col-sm-10">
						<input type="file" class="form-control round-form" name="foto">
					</div>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{$data->idbuku}}"></input>
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
