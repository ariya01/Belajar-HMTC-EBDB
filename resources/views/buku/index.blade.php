@extends('master')

@section('content')
<h3 class=" text-center">Buku</h3>
<br>
@if($data==0)
<h4 class="text-center">Data Belum ada yang dimasukan</h4>
@else
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>Id Buku</th>
						<th>Nama Buku</th>
						<th>Jumlah</th>
						<th>Foto</th>
						<th>Sisa</th>
						<th>Perintah</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $a)
					<tr>
						<td>{{$a->idbuku}}</td>
						<td>{{$a->namabuku}}</td>
						<td>{{$a->jumlah}}</td>
						<td><p class="control-label"><img src="{{ asset('foto/buku/'.$a->foto) }}" height="150" width="150"></p></td>
						<td>{{$a->jumlah-$a->dipinjam+$a->kembali}}</td>
						<td>
							<form method="post" action="{{url('hapus_buku')}}">
								<input type="hidden" name="id" value="{{$a->idbuku}}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">	
								<button class="btn btn-primary btn-xs" value="1" name="akses" type="submit"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger btn-xs" value="2" name="akses" type="submit"><i class="fa fa-trash-o "></i></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div><!-- /content-panel -->
	</div><!-- /col-md-12 -->
</div><!-- /row -->
@endif
@endsection
