@extends('master')
@section('content')
<h3 class=" text-center">Form Buku</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('masuk_buku')}}" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Id Buku</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="idbuku">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Buku</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="nama">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Jumlah Buku</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="jumlah">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Edisi Buku</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="edi">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Foto</label>
					<div class="col-sm-10">
						<input type="file" class="form-control round-form" name="foto">
					</div>
				</div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
