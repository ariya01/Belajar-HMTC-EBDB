@extends('master')
@section('content')
<h3 class=" text-center">Edit Buku</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('update_keluarga')}}">
				<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Id Status</label>
					<div class="col-sm-10">
						<input class="form-control round-form" id="disabledInput" type="text" placeholder="{{$data->idke}}" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Status</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="namake" placeholder="{{$data->namake}}">
					</div>
				</div>
				<input type="hidden" name="id" value="{{$data->idke}}"></input>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
