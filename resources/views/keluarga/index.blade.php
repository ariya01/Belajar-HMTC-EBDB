@extends('master')

@section('content')
<h3 class=" text-center">Keluarga</h3>
<br>
@if($data==0)
<h4 class="text-center">Data Belum ada yang dimasukan</h4>
@else
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>Id Keluarga</th>
						<th>Nama Keluarga</th>
						<th>Perintah</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data2 as $a)
					<tr>
						<td>{{$a->idke}}</td>
						<td>{{$a->namake}}</td>
						<td>
							<form method="post" action="{{url('hapus_keluarga')}}">
								<input type="hidden" name="id" value="{{$a->idke}}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">	
								<button class="btn btn-primary btn-xs" value="1" name="akses" type="submit"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger btn-xs" value="2" name="akses" type="submit"><i class="fa fa-trash-o "></i></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div><!-- /content-panel -->
	</div><!-- /col-md-12 -->
</div><!-- /row -->
@endif
@endsection
