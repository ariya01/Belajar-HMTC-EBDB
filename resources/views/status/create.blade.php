@extends('master')
@section('css')
.alert {
    height: 30px;
    margin: 0 0 20px;
    background: #ccc;
}
.alert:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -5px;
}

.alert .centered {
    display: inline-block;
    vertical-align: middle;
    padding: 0 10px 0 15px;
}
}
@endsection
@section('content')
<h3 class=" text-center">Form Status</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('masuk_status')}}">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Id Status</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="idstatus">
						@if ($errors->has('pesan'))
							<div class="alert alert-danger"><span>
								{{$errors->first()}}
							</span></div>
						@endif
					</div>

				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Status</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="namastatus">
					</div>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
