@extends('master')

@section('content')
<h3 class=" text-center">Curhatan</h3>
<br>
@if($data==0)
<h4 class="text-center">Data Belum ada yang dimasukan</h4>
@else
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>Nama</th>
						<th>Nrp</th>
						<th>Topik</th>
						<th>Curhat</th>
					</tr>
				</thead>
				<tbody>
				@foreach($data2 as $a)
				<tr>
					<td>{{$a->nama}}</td>
					<td>{{$a->nrp}}</td>
					<td>{{$a->topik}}</td>
					<td>{{$a->curhat}}</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div><!-- /content-panel -->
	</div><!-- /col-md-12 -->
</div><!-- /row -->
@endif
@endsection
