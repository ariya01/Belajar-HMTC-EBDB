@extends('master')
@section('content')
<h3 class=" text-center">Form Publikasi</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('masuk_publi')}}" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Biasiswa</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="nama">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Tanggal Diterma</label>
					<div class="col-sm-10">
						<input type="date" class="form-control round-form" name="tanggal">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Keterangan</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="keterangan">
					</div>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection