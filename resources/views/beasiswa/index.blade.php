@extends('master')

@section('content')
<h3 class=" text-center">Progres Biasiswa</h3>
<br>
@if($data==0)
<h4 class="text-center">Data Belum ada yang dimasukan</h4>
@else
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>Nama Beasiswa</th>
						<th>Tanggal</th>
						<th>Status</th>
						<th>Keterangan</th>
						<th>Edit Status</th>
						<th>Perintah</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data2 as $a)
					<tr>
						<td>{{$a->namabeasiswa}}</td>
						<td>{{$a->tanggal}}</td>
						<td>
						@if($a->progres==0) 
						<div>
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-danger"  role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%">
									<span class="sr-only">25% Complete</span>
								</div>
							</div>
						</div><!-- /showback -->
						@elseif($a->progres==1)
						<div>
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
									<span class="sr-only">50% Complete</span>
								</div>
							</div>
						</div><!-- /showback -->
						@elseif($a->progres==2)
						<div>
							<div class="progress progress-striped active">
								<div class="progress-bar"  role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
									<span class="sr-only">75% Complete</span>
								</div>
							</div>
						</div><!-- /showback -->
						@else
						<div>
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									<span class="sr-only">100% Complete</span>
								</div>
							</div>
						</div><!-- /showback -->
						@endif
						</td>
						<td>@if($a->keterangan==null)
						Belum Dimasukan
						@else
						{{$a->keterangan}}
						@endif
						</td>
						<td>
							<form method="post" action="{{url('hapus_publi')}}">
								<input type="hidden" name="id" value="{{$a->id}}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">	
								<button class="btn btn-danger btn-xs" value="0" name="akses" type="submit">1</button>
								<button class="btn btn-warning btn-xs" value="1" name="akses" type="submit">2</button>
								<button class="btn btn-primary btn-xs" value="2" name="akses" type="submit">3</button>
								<button class="btn btn-success btn-xs" value="3" name="akses" type="submit">4</button>
							
						</td>
						<td>
							<button class="btn btn-danger btn-xs" value="4" name="akses" type="submit"><i class="fa fa-trash-o "></i></button>
							<button class="btn btn-primary btn-xs" value="5" name="akses" type="submit"><i class="fa fa-pencil "></i></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div><!-- /content-panel -->
	</div><!-- /col-md-12 -->
</div><!-- /row -->
@endif
@endsection
