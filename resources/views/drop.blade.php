@extends('master')

@section('content')
<h3 class=" text-center">Mahasiswa Sudah Di Drop</h3>
<br>
@if($data==0)
<h4 class="text-center">Tidak Ada Mahaiswa Yang Absen Lebih dari 2</h4>
@else
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>NRP</th>
						<th>Nama Mahasiswa</th>
						<th>Kelas</th>
						<th>Absen</th>
					</tr>
				</thead>
				<tbody>
					@foreach($user as $a)
					<tr>
						<td>{{$a->nrp}}</td>
						<td>{{$a->nama}}</td>
						<td>{{$a->kelas}}</td>
						<td>{{$a->status}} kali</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div><!-- /content-panel -->
	</div><!-- /col-md-12 -->
</div><!-- /row -->
@endif
@endsection
