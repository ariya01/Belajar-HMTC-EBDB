@extends('master')

@section('content')
<h3 class=" text-center">Absen</h3>
<br>
@if($data==0)
<h4 class="text-center">Data Belum ada yang dimasukan</h4>
@else
<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-body">
				<form method="GET">
					Filter berdasarkan kelas
					<select class="form-inline" name="kelas" required>
						<option value="" selected="" disabled="">Semua Kelas</option>
						@foreach ($list as $list)
						<option value="{{$list->idkelas}}">{{$list->kelas}}</option>
						@endforeach
					</select>
					<input type="submit" name="" value="pilih" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
</div>
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>NRP</th>
						<th>Nama Mahasiswa</th>
						<th>Kelas</th>
						<th>Perinatah</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data2 as $a)
					<tr>
						<td>{{$a->nrp}}</td>
						<td>{{$a->nama}}</td>
						<td>{{$a->kelas}}</td>
						<td>
							<form method="post" action="{{url('hapus_absen')}}">
								<input type="hidden" name="id" value="{{$a->nrp}}">
								<input type="hidden" name="idkelas" value="{{$a->idkelas}}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">	
								<button class="btn btn-primary btn-xs" value="1" name="akses" type="submit"><i class="fa fa-pencil"></i></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div><!-- /content-panel -->
	</div><!-- /col-md-12 -->
</div><!-- /row -->
@endif
@endsection
