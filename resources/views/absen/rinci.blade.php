@extends('master')
@section('content')
<h3 class=" text-center">Data Rincian Absen</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<div class="form-horizontal style-form">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data3->nama}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">NRP</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data3->nrp}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Kelas</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data3->kelas}}</p>
					</div>
				</div>
				@if($data2==0)
				<h4 class="text-center">Belum Diabsen</h4>
				@else
				<br>
				@foreach($data4 as $a)
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Pekan {{$a->pekan}}</label>
					<div class="col-sm-10">
						<p class="control-label">
							<div class="col-md-6">
								@if($a->pekanstatus==1)
								<span class="label label-success">Masuk</span>
								@else
								<span class="label label-danger">Tidak Masuk</span>
								@endif	
							</div>
							<div class="col-md-6">
								<form action="{{url('pekan')}}" method="post">
									<button class="btn btn-round btn-success" value="1" name="akses" type="submit"><i class="fa fa-check"></i></button>
									<button class="btn btn-round btn-danger" value="2" name="akses" type="submit"><i class="fa fa-times"></i></button>
									<button class="btn btn-round btn-danger" value="3" name="akses" type="submit"><i class="fa fa-trash "></i></button>
									<input type="hidden" name="id" value="{{$a->id}}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</form>
							</div>
						</p>
					</div>
				</div>
				@endforeach
				@endif
				Tambah pekan Ke {{++$data2}}
				<br>
				<form action="{{url('tambah')}}" method="post">
					<button type="submit" class="btn btn-round btn-primary" value="1" name="akses"><i class="fa fa-check"></i>Masuk</button>
					<button type="submit" class="btn btn-round btn-danger" value="2" name="akses"><i class="fa fa-times"></i>Tidak Masuk</button>
					<input type="hidden" name="id" value="{{$data3->nrp}}">
					<input type="hidden" name="idkelas" value="{{$data3->idkelas}}">
					<input type="hidden" name="pekan" value="{{$data2}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				</form>
			</div>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->


@endsection
