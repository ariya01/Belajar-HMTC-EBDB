@extends('master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="http://position-absolute.com/creation/print/jquery.printPage.js"></script>
	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<!--external css-->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
</head>
<body>
	<h3 class=" text-center">Mahasiswa</h3>
	<!-- <input type="text" name="form-control" id="search" name="search"> -->
	<div class="col-md-5">
		<div class="form-group">
			<label class="col-sm-2 col-sm-2 control-label">Cari</label>
			<div class="col-sm-10">
				<input type="text" class="form-control round-form " id="search" name="search">
			</div>
		</div>
		
	</div>
	<div class="row mt">
		<div class="col-md-12">
			<div class="content-panel">
				<table class="table table-striped table-advance table-hover">
					<thead>
						<tr>
							<th>NRP</th>
							<th>Nama Mahasiswa</th>
							<th>UKT</th>
							<th>Status</th>
							<th>Keluarga</th>
							<th>Penghasilan Orangtua</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data3 as $a)
						<tr>
							<td>{{$a->nrp}}</td>
							<td>{{$a->nama}}</td>
							<td>{{$a->ukt}}</td>
							<td>{{$a->namastat}}</td>
							<td>{{$a->namake}}</td>
							<td>{{$a->pekortu}}</td>
						</tr>
						@endforeach 
					</tbody>
				</table>
			</div><!-- /content-panel -->
		</div><!-- /col-md-12 -->
	</div><!-- /row -->
	<script type="text/javascript">
		$('#search').on('keyup',function(){
			$value=$(this).val();
			$.ajax({
				type : 'get',
				url  : '{{URL::to('search')}}',
				data : {'search':$value},
				success : function(data){
					$('tbody').html(data);
				}
			})
		})
	</script>
</body>
</html>
@endsection