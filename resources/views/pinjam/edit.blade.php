@extends('master')
@section('content')
<h3 class=" text-center">Form Peminjam</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('update_pinjam')}}" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">NRP</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="nrp" placeholder="{{$data->nrp}}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Buku</label>
					<div class="col-sm-10">
						<select class="form-control round-form" name="buku">
			              <option value="0" disabled="true" selected="true">Pilih Buku</option>
			              @foreach($buku as $a)
			              <option value="{{$a->idbuku}}">{{$a->namabuku}}</option>
			              @endforeach
			            </select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">NRP</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="id" placeholder="{{$data->Idnya}}">
					</div>
				</div>
				<input type="hidden" name="id" value="{{$data->id}}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
