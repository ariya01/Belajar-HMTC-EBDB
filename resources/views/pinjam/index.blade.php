@extends('master')

@section('content')
<h3 class=" text-center">Pinjam</h3>
<br>
@if($data==0)
<h4 class="text-center">Data Belum ada yang dimasukan</h4>
@else
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>Id Pinjam</th>
						<th>Nrp</th>
						<th>Buku</th>
						<th>Id Buku</th>
						<th>Status</th>
						<th>Perintah</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data2 as $a)
					<tr>
						<td>{{$a->id}}</td>
						<td>{{$a->nrp}}</td>
						<td>{{$a->namabuku}}</td>
						<td>{{$a->Idnya}}</td>
						<th>@if($a->kembali==1) 
							<div>
								<span class="label label-success">Sudah Dikembalikan</span>
							</div>
							@else
							<div>
								<span class="label label-danger">Belum Dikembalikan</span>
							</div>
							@endif</th>
							<td>
								<form method="post" action="{{url('hapus_pinjam')}}">
									<input type="hidden" name="id" value="{{$a->id}}">
									<input type="hidden" name="idbuku" value="{{$a->idbuku}}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">	
									<button class="btn btn-primary btn-xs" value="1" name="akses" type="submit"><i class="fa fa-pencil"></i></button>
									<button class="btn btn-danger btn-xs" value="2" name="akses" type="submit"><i class="fa fa-trash-o "></i></button>
									@if($a->kembali==0)
									<button class="btn btn-success btn-xs" value="3" name="akses" type="submit"><i class="fa fa-check "></i></button>
									@else
									<button class="btn btn-danger btn-xs" value="4" name="akses" type="submit"><i class="fa fa-times "></i></button>
									@endif
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div><!-- /content-panel -->
		</div><!-- /col-md-12 -->
	</div><!-- /row -->
	@endif
	@endsection
