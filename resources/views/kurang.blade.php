@extends('master')

@section('content')
<h3 class=" text-center">Mahasiswa</h3>
<br>
@if($kurang==0)
<h4 class="text-center">Belum ada yang dicari</h4>
@else
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>NRP</th>
						<th>Nama Mahasiswa</th>
						<th>UKT</th>
						<th>Status</th>
						<th>Keluarga</th>
						<th>Penghasilan Orangtua</th>
						<th>Jumlah Saudara</th>
					</tr>
				</thead>
				<tbody>
					@foreach($kurang1 as $a)
					<tr>
						<td>{{$a->nrp}}</td>
						<td>{{$a->nama}}</td>
						<td>{{$a->ukt}}</td>
						<td>{{$a->namastat}}</td>
						<td>{{$a->namake}}</td>
						<td>{{$a->pekortu}}</td>
						<td>{{$a->kakak+$a->adik}}</td>
					</tr>
					@endforeach 
				</tbody>
			</table>
		</div><!-- /content-panel -->
	</div><!-- /col-md-12 -->
</div><!-- /row -->
@endif

@endsection
