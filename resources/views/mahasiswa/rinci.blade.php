@extends('master')
@section('css')
.alert {
    height: 30px;
    margin: 0 0 20px;
    background: #ccc;
}
.alert:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -5px;
}

.alert .centered {
    display: inline-block;
    vertical-align: middle;
    padding: 0 10px 0 15px;
}
}
@endsection
@section('content')
<h3 class=" text-center">Form Mahasiswa</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<div class="form-horizontal style-form">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">NRP</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->nrp}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Mahasiswa</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->nama}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Tempat Lahir</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->lahir}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Tanggal Lahir</label>
					<div class="col-sm-10">
						<p class="control-label">{{date("d/m/Y", strtotime($data->tanggal))}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Alamat Surabaya</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->domisi}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Beasiswa</label>
					<div class="col-sm-10">
					<p class="control-label">{{$data->namastat}}</p>	
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Keluarga</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->namake}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Telephone</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->telp}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">UKT</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->ukt}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Jumlah Kakak</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->kakak}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Jumlah Adik</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->adik}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Ayah</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->Ayah}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Ibu</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->Ibu}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Alamat Ortu</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->alamat}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Pekerjaan Ayah</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->pekayah}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Pekerjaan Ibu</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->pekibu}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Pengahasilan Ortu</label>
					<div class="col-sm-10">
						<p class="control-label">{{$data->pekortu}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Foto Rumah</label>
					<div class="col-sm-10">
						<p class="control-label"><img src="{{ asset($hasil) }}" height="150" width="150"></p>
					</div>
				</div>
				</div>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
 