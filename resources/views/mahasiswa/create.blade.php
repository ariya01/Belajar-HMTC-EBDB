@extends('master')
@section('mahasiswa')
class="active"
@endsection
@section('css')
.alert {
    height: 30px;
    margin: 0 0 20px;
    background: #ccc;
}
.alert:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -5px;
}

.alert .centered {
    display: inline-block;
    vertical-align: middle;
    padding: 0 10px 0 15px;
}
}
@endsection
@section('content')
<h3 class=" text-center">Form Mahasiswa</h3>
<br>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<form class="form-horizontal style-form" method="post" action="{{url('masuk_mahasiswa')}}" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">NRP</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="nrp">
						@if ($errors->has('pesan'))
							<div class="alert alert-danger"><span>
								{{$errors->first()}}
							</span></div>
						@endif
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Mahasiswa</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="nama">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Tempat Lahir</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="tempat">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Tanggal Lahir</label>
					<div class="col-sm-10">
						<input type="date" class="form-control round-form" name="tanggal">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Alamat Surabaya</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="surabaya">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Beasiswa</label>
					<div class="col-sm-10">
						<select class="form-control round-form" name="beasiswa" id="beasiswa">
			              <option value="0" disabled="true" selected="true">Pilih Beasiswa</option>
			              @foreach($data as $a)
			              <option value="{{$a->idstat}}">{{$a->namastat}}</option>
			              @endforeach
			            </select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Keluarga</label>
					<div class="col-sm-10">
						<select class="form-control round-form" name="keluarga" id="keluarga">
			              <option value="0" disabled="true" selected="true">Pilih Keluarga</option>
			              @foreach($data2 as $a)
			              <option value="{{$a->idke}}">{{$a->namake}}</option>
			              @endforeach
			            </select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Telephone</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="telp">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">UKT</label>
					<div class="col-sm-10">
						<select class="form-control round-form" name="ukt" id="ukt">
			              <option value="0" disabled="true" selected="true">Pilih UKT</option>
			              <option value="500000">Rp 0,00</option>
			              <option value="500000">Rp 500.000,00</option>
			              <option value="1000000">Rp 1.000.000,00</option>
			              <option value="2500000">Rp 2.500.000,00</option>
			              <option value="4000000">Rp 4.000.000,00</option>
			              <option value="5000000">Rp 5.000.000,00</option>
			              <option value="6000000">Rp 6.000.000,00</option>
			              <option value="7500000">Rp 7.500.000,00</option>
			            </select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Jumlah Kakak</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="kakak">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Jumlah Adik</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="adik">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Ayah</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="namaayah">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Nama Ibu</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="namaibu">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Alamat Ortu</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="alamat">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Pekerjaan Ayah</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="pekayah">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Pekerjaan Ibu</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form" name="pekibu">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Pengahasilan Ortu</label>
					<div class="col-sm-10">
						<select class="form-control round-form" name="penghasilan" id="ukt">
			              <option value="0" disabled="true" selected="true">Pilih Pengahasilan</option>
			              <option value="500000">< Rp 1.000.000,00</option>
			              <option value="1500000">Rp 1.000.000,00 - Rp 2.000.000,00</option>
			              <option value="2500000">Rp 2.000.000,00 - Rp 3.000.000,00</option>
			              <option value="3500000">Rp 3.000.000,00 - Rp 4.000.000,00</option>
			              <option value="4500000">Rp 4.000.000,00 - Rp 5.000.000,00</option>
			              <option value="5500000">Rp 5.000.000,00 - Rp 6.000.000,00</option>
			              <option value="6500000">Rp 6.000.000,00 - Rp 7.000.000,00</option>
			              <option value="7500000">> Rp 7.000.000,00</option>
			            </select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Foto Rumah</label>
					<div class="col-sm-10">
						<input type="file" class="form-control round-form" name="photo">
					</div>
				</div>
				<hr>
				<button type="submit" class="btn btn-round btn-primary">Submit</button>
			</form>
		</div>
	</div><!-- col-lg-12-->      	
</div><!-- /row -->
@endsection
