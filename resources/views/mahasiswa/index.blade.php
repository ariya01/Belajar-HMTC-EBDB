@extends('master')

@section('content')
<h3 class=" text-center">Mahasiswa</h3>
<br>
@if($data==0)
<h4 class="text-center">Data Belum ada yang dimasukan</h4>
@else
<div class="row mt">
	<div class="col-md-12">
		<div class="content-panel">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th>NRP</th>
						<th>Nama Mahasiswa</th>
						<th>UKT</th>
						<th>Status</th>
						<th>Keluarga</th>
						<th>Penghasilan Orangtua</th>
						<th>Jumlah Saudara</th>
						<th>Perintah</th>
						<th>Lihat</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data3 as $a)
					<tr>
						<td>{{$a->nrp}}</td>
						<td>{{$a->nama}}</td>
						<td>{{$a->ukt}}</td>
						<td>{{$a->namastat}}</td>
						<td>{{$a->namake}}</td>
						<td>{{$a->pekortu}}</td>
						<td>{{$a->kakak+$a->adik}}</td>
						<td>
							<form method="post" action="{{url('hapus_mahasiswa')}}">
								<input type="hidden" name="id" value="{{$a->nrp}}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">	
								<button class="btn btn-primary btn-xs" value="1" name="akses" type="submit"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger btn-xs" value="2" name="akses" type="submit"><i class="fa fa-trash-o "></i></button>
							
						</td>
						<td>
								<button class="btn btn-success btn-xs" value="3" name="akses" type="submit"><i class="fa fa-cog fa-spin"></i></button>
								</form>
						</td>
					</tr>
					@endforeach 
				</tbody>
			</table>
		</div><!-- /content-panel -->
	</div><!-- /col-md-12 -->
</div><!-- /row -->
@endif

@endsection
