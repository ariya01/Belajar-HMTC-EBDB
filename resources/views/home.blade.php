@extends('master')
@section('css')
.img-center {margin:0 auto;}
.img-responsive {margin:0 auto;}
@endsection
@section('home')
class="active"
@endsection
@section('content')
<h1 class="text-center">Status</h2>
<br>
<div class="row">
	<div class="col-md-12">
		<img class="img-center img-responsive" src="images/logo.jpg" height="150" width="150">
	</div>
</div>
<h3>Absen</h3>
<a href="{{url('calendar')}}">
	<div class="row" >
		<div class="col-lg-4 col-md-4 col-sm-4 mb">
			<div class="weather-3 pn centered">
				<i class="fa fa-calendar"></i>
				<h1>{{$datediff}}</h1>
				<div class="info">
					<div class="row">
						<h3 class="centered">Minggu</h3>
						<div class="col-sm-6 col-xs-6 pull-left">
							<p class="goleft"><i class="fa fa-tint"></i></p>
						</div>
						<div class="col-sm-6 col-xs-6 pull-right">
							<p class="goright"><i class="fa fa-flag"></i></p>
						</div>
					</div>
				</div>
			</div>
		</div><! --/col-md-4 -->
</a>
	<a href="{{url('warning')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="bahaya-panel pn">
				<div class="bahaya-header">
					<h5>Warning</h5>
				</div>
				<h1 class="mt"><i class="fa fa-exclamation-triangle fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$warning}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
	<a href="{{url('drop')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="drop-panel pn">
				<div class="drop-header">
					<h5>Sudah Drop</h5>
				</div>
				<h1 class="mt"><i class="fa fa-sign-out fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$drop}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
</div>
<h3>Mahasiswa</h3>
<div class="row">
	<a href="{{url('mahasiswa')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Jumlah Mahasiswa</h5>
				</div>
				<h1 class="mt"><i class="fa fa-users fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$data}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
	<a href="{{url('kurang')}}"> 	 
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Mahasiswa Menengah Kebawah</h5>
				</div>
				<h1 class="mt"><i class="fa fa-eye fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$kurang}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
	<a href="{{url('bidik')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Mahasiswa bidik Misi</h5>
				</div>
				<h1 class="mt"><i class="fa fa-handshake-o fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$bidik}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->	
	</a>
</div>
<h3>Buku</h3>	
<div class="row">
	<a href="{{url('buku')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Jumlah Buku</h5>
				</div>
				<h1 class="mt"><i class="fa fa-book fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$jumlah->jumlah-$pinjam+$kembali}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
	<a href="{{url('belum')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Buku Yang Terpinjam</h5>
				</div>
				<h1 class="mt"><i class="fa fa-exclamation fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$pinjam}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
	<a href="{{url('sudah')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Buku Yang Kembali</h5>
				</div>
				<h1 class="mt"><i class="fa fa-refresh fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$kembali}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>	
</div>
<h3>Beasiswa</h3>	
<div class="row">
	<a href="{{url('belum_publi')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Biasiswa Yang Diterima Dan Belum Ditindak lanjuti</h5>
				</div>
				<h1 class="mt"><i class="fa fa-upload fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$publi1}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
	<a href="{{url('medfo')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Sudah Berada Di Medfo</h5>
				</div>
				<h1 class="mt"><i class="fa fa-sticky-note-o fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$publi2}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
	<a href="{{url('bebas')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Sudah Di Publikasi</h5>
				</div>
				<h1 class="mt"><i class="fa fa-check-square-o fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$publi3}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>	
</div>
<h3>Curhat</h3>	
<div class="row">
	<a href="{{url('lihatcur')}}">
		<div class="col-md-4 col-sm-4 mb">
			<div class="darkblue-panel pn">
				<div class="darkblue-header">
					<h5>Curhatan</h5>
				</div>
				<h1 class="mt"><i class="fa fa-puzzle-piece fa-3x"></i></h1>
				<p></p>
				<footer>
					<div class="centered">
						<h5>{{$curhat}}</h5>
					</div>
				</footer>
			</div><! -- /darkblue panel -->
		</div><!-- /col-md-4 -->
	</a>
</div>

@endsection
